#!/bin/bash
# ==================================================
# Project: System Activity Reporter Checker
# Filename: sysactivity.sh
# Author: Lee Schroeder <spaceseel at gmail dot com>
# License: GNU GPLv3
# ==================================================

# The echo is not needed, but it helps with readability
sudo echo

# Show the memory first
printf "=============\n"
printf "Memory Stats:\n"
printf "=============\n\n"
sar -r 2 4

# Central Processing Unit Status
printf "\n==========\n"
printf "CPU Stats:\n"
printf "==========\n\n"
sar -u ALL 2 2

# Input/Output Status
printf "\n==========\n"
printf "I/O Stats:\n"
printf "==========\n\n"
sar -b 1 4

# Hard Disk Status
printf "\n===========\n"
printf "Disk Stats:\n"
printf "===========\n\n"
sar -d 2 2

# Network Status
printf "\n==============\n"
printf "Network Stats:\n"
printf "==============\n\n"
sar -n DEV 2 2

# Print a blank line for readability
echo

# Exit the script when done
exit
