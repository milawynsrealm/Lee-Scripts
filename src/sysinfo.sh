#!/bin/bash
# ==================================================
# Project: Output system information to HTML
# Filename: sysinfo.sh
# Author: Lee Schroeder <spaceseel at gmail dot com>
# License: GNU GPLv3
# ==================================================
sysFile=$(date +"sysinfo_%Y_%m_%d__%H_%M_%S.html")

printf "Generating HTML file ($sysFile)...\n"
sudo lshw -html > $sysFile
