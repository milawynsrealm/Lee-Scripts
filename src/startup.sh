#!/bin/bash
# ==================================================
# Project: Startup Script
# Filename: startup.sh
# Author: Lee Schroeder <spaceseel at gmail dot com>
# License: GNU GPLv3
# ==================================================
clear

# Get the name of the current host
hname=$(uname -n)
huser=$(whoami)
printf "\e[0;31;40m$huser@$hname\n\e[0;31;0m"

# ==================================================

# Get the user friendly name of the current distribution
osName=$(grep '^PRETTY_NAME' /etc/os-release | tr -d '"')
osArch=$(uname -p)
printf "\e[0;93;40mOS:\e[0;31;0m "
printf "%s (%s)\n" "${osName:12}" "${osArch}"

# ==================================================

# Get the kernel type and version
kerName=$(uname -o)
kerVer=$(uname -r)
printf "\e[0;93;40mKernel:\e[0;31;0m "
printf "%s (%s)\n" "$kerName" "$kerVer"

# ==================================================

# Get the uptime
upAmt=$(uptime -p)
upTmAmt=$(uptime -s)
printf "\e[0;93;40mUptime:\e[0;31;0m "
printf "%s - (Since %s)\n" "${upAmt:3}" "${upTmAmt}"

# ==================================================

# Show which shell being used. Bash is default, but it has to
# be assumed that the user might be using another kind of shell.
shlType=""

# Print the header
printf "\e[0;93;40mShell:\e[0;31;0m "

# Find out which shell is being used
if [ -n "$BASH_VERSION" ]
then
    # For the BASH shell
    shlType=$BASH_VERSION
elif [ -n "$ZSH_VERSION" ]
then
    # For the zsh shell
    shlType=$ZSH_VERSION
elif [ -n "$ZSH_VERSION" ]
then
    # For the ksh shell or some other shells
    shlType=$KSH_VERSION
elif [ -n "$version" ]
then
    # For the tcsh or fish shell (or another?)
    shlType=$version
else
    # Another shell is being used. Since each shell is different,
    # the variable used may be unknown and left out intentially.
    shlType=$(which $SHELL)
fi

# Show the user what shell is being used
printf "%s (%s)\n" "$SHELL" "${shlType}"

# ==================================================

cpuName=$(tty)
printf "\e[0;93;40mTerminal:\e[0;31;0m %s\n" "$cpuName"

# ==================================================

# Grabs the name of the CPU the system is running on
cpuName=$(lscpu | sed -nr '/Model name/ s/.*:\s*(.*) @ .*/\1/p')
printf "\e[0;93;40mCPU:\e[0;31;0m %s\n" "$cpuName"

# ==================================================

# Shows the current date and time
dTime=$(date +"%A, %Y/%m/%d - %H:%M:%S %p")
printf "\e[0;93;40mDate/Time:\e[0;31;0m %s\n" "$dTime"


# ==================================================

# Get basic information about the system's memory
memTotalCnt=$(grep MemTotal /proc/meminfo)
memFreeCnt=$(grep MemFree /proc/meminfo)
printf "\e[0;93;40mMemory:\e[0;31;0m %s / %s\n" "${memFreeCnt:18}" "${memTotalCnt:16}"

# ==================================================

# If the user is currently in root mode, make sure they know
ROOT_UID=0
if [ "$UID" -eq "$ROOT_UID" ]
then
printf "\e[0;31;40m * WARNING: You are the root user. Be Careful!!\e[0;31;0m\n"
fi

# Empty line to make it easier to tell where the input line is
printf "\n"

# Exits the script
exit
