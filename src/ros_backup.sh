#!/bin/bash
# ==================================================
# Project: ReactOS Backup Script
# Filename: ros_backup.sh
# Author: Lee Schroeder <spaceseel at gmail dot com>
# License: GNU GPLv3
# ==================================================
printf "Backing up ReactOS Source Code...\n"

# Make sure there is a directory to work with
if [ ! -d "../reactos/" ]
then
    printf "ReactOS directory not found! Stopping script!\n\n"
    exit 0
fi

# Get the date for the file timestamp
curDate=$(date +"%Y_%m_%d__%H_%M_%S")

# If the removable media is not found, then just create it locally
if [ ! -d "/media/milawynsrealm/Gallus/reactos backup/" ]
then
    # Make sure the output directory is prepared
    if [ ! -d "./ROS Backup" ]
    then
        mkdir -v "./ROS Backup"
    fi

    # Set the current directory to the backup one if the first one cannot be found
    curDir='./ROS Backup'
    printf "Removable media 'Gallus' not found. Storing it locally instead to './ROS Backup.'\n"
else
    # Set to the remote location as the output location
    curDir='/media/milawynsrealm/Gallus/reactos backup'
fi

# Make the backup archive. Leave out the .git folder since it
# takes up so much space, and would be overwritten later on,
# making it useless to other users.
7z a "${curDir}/reactos_${curDate}.7z" "../reactos/" -xr!.git

# Exit the script
exit
