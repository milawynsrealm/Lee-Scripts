#!/bin/bash
# ==================================================
# Project: Hard Drive Sizes
# Filename: hdsize.sh
# Author: Lee Schroeder <spaceseel at gmail dot com>
# License: GNU GPLv3
# ==================================================

# Show the header so the user can know which each part of the table means
df -hT | head -n 1
printf "====================================================================\n"

# get all the connected partitions and filter out only the ones that are
# the ones that the typical user would care about.
df -hT | grep '^/dev/sd*'

# Add a blank line for readability
echo

exit 0
