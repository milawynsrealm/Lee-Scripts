# Lee's Personal Scripts

Miscellaneous scripts written for my own use. It's mostly here for resume purposes. These scripts have been written with [Kubuntu](https://kubuntu.org/) in mind, so if you use another distribution or a different shell, please keep that in mind please. Also, since the scripts stored here have not been fully tested and I am stil in the process of learning, no gurrantees are provided.
